KUBE_NAMESPACE?=testnamespace
CI_COMMIT_SHA?=local
VERSION?=$(shell cat package.json | grep version | head -1 | awk -F: '{ print $$2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
HELM_RELEASE?=taranta

# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker
STAGE ?= build_react_artefacts
PROJECT = ska-tango-taranta

JS_PROJECT_DIR ?= ./taranta
JS_PACKAGE_MANAGER ?= npm
JS_ESLINT_CONFIG ?= .eslintrc.json
JS_TEST_COMMAND ?= react-scripts test
JS_TEST_SWITCHES ?=

#Taranta default vars:
MIN_WIDGET_SIZE ?= 10
WIDGETS_TO_HIDE ?= ['SARDANA_MOTOR','MACROBUTTON']
SHOW_COMMAND_FILE_ON_DEVICES ?= true
ENVIRONMENT ?= true

ifneq ($(MINIKUBE),)
ifneq ($(MINIKUBE),true)
K8S_CHART_PARAMS = --set global.aws_url=$(AWS_URL)
endif
endif

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include core make support
include .make/base.mk

# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include js support
include .make/js.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

oci-pre-build:
	@sed -i "/MIN_WIDGET_SIZE/s/: 20/: $(MIN_WIDGET_SIZE)/" taranta/public/config.js
	@sed -i "/WIDGETS_TO_HIDE/s/: \\[\\]/: $(WIDGETS_TO_HIDE)/" taranta/public/config.js
	@sed -i "/SHOW_COMMAND_FILE_ON_DEVICES/s/: false/: $(SHOW_COMMAND_FILE_ON_DEVICES)/" taranta/public/config.js
	@sed -i "/environment/s/: false/: $(ENVIRONMENT)/" taranta/public/config.js
	@sed -i "/FETCH_COMM_HEALTH/s/: 60/: $(FETCH_COMM_HEALTH)/" taranta/public/config.js
	@cat taranta/public/config.js

build-docs:
	docker run --rm -d -v $(PWD):/tmp -w /tmp/docs netresearch/sphinx-buildbox sh -c "make html"
