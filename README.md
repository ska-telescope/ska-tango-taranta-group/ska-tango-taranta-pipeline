# ska-tango-taranta-pipeline

- SKA ska-tango-taranta Pipeline is a project created to run all the specific tests and CI/CD requirements for the SKA project

- If you are looking for the actual code of taranta please go to official repo https://gitlab.com/tango-controls/web/taranta

- Documentation available at https://taranta.readthedocs.io/
 
- This project CI/CD is set to run every time a new push is done on the main branch of the official repo

- Optionally a developer can trigger the pipeline manually and make use of the BRANCH var to set a specific branch for the pipeline to run

<img src='https://taranta.readthedocs.io/en/sp-1406/_static/img/taranta_logo.png'>