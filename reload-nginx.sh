#!/bin/sh
while true; do
    sleep 1800
    if pgrep nginx > /dev/null; then
        nginx -s reload
    else
        exit 0
    fi
done