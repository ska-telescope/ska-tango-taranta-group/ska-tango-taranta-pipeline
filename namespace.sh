#!/bin/sh

# Modify the index.html file based on the environment variable
# Check if $NAMESPACE is set and not empty
if [ -n "$NAMESPACE" ]; then
  # Read the content of config.js into a variable
  echo ${NAMESPACE} > /usr/share/nginx/html/namespace.txt
  sed -i "s|\"basename\": \"\"|\"basename\": \"/$NAMESPACE\"|g" /usr/share/nginx/html/config.js
  sed -i "s|\"TANGO_DATABASES\": \[\]|\"TANGO_DATABASES\": $TANGO_DBS|g" /usr/share/nginx/html/config.js
   # Read the modified content of config.js into a variable
  CONFIG_JS_CONTENT=$(cat /usr/share/nginx/html/config.js)

  # Escape special characters in CONFIG_JS_CONTENT
  CONFIG_JS_CONTENT=$(echo "$CONFIG_JS_CONTENT" | sed 's/\\/\\\\/g; s/"/\\"/g; s/&/\\&/g')
  CONFIG_JS_CONTENT=$(echo "$CONFIG_JS_CONTENT" | awk '{printf "%s\\n", $0}')

  # Replace the script tag in index.html with the modified config.js content
  sed -i "s|<script type=\"text/javascript\" src=\"/config.js[^\"]*\"></script>|<script type=\"text/javascript\">\n${CONFIG_JS_CONTENT}\n</script>|g" /usr/share/nginx/html/index.html

  sed -i "s|/static|/$NAMESPACE/static|g" /usr/share/nginx/html/index.html
  sed -i "s|/manifest.json|manifest.json|g" /usr/share/nginx/html/index.html
  sed -i "s|namespace|$NAMESPACE|g" /etc/nginx/nginx.conf

  AWS_URL=${AWS_URL:-"https://k8s-services.skao.int"}
  sed -i "s|aws_service_url|${AWS_URL}|g" /etc/nginx/nginx.conf

  CSS_FILE=$(ls /usr/share/nginx/html/static/css/main.*.css)
  if [ -n "$CSS_FILE" ]; then
    # Perform the replacement in the CSS file
    sed -i "s|/static|/$NAMESPACE/static|g" "$CSS_FILE"
  fi
fi

# Replace worker_processes and worker_connections in nginx.conf
if [ -n "$NGINX_WORKER_PROCESSES" ]; then
  sed -i "s|worker_processes .*;|worker_processes $NGINX_WORKER_PROCESSES;|g" /etc/nginx/nginx.conf
fi

if [ -n "$NGINX_WORKER_CONNECTIONS" ]; then
  sed -i "s|worker_connections .*;|worker_connections $NGINX_WORKER_CONNECTIONS;|g" /etc/nginx/nginx.conf
fi

#ngnix reload
/etc/periodic/1800sec/reload-nginx & 

# Start the primary process (nginx in foreground mode)
exec nginx -g "daemon off;"
