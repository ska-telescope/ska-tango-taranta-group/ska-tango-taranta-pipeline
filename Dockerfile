# Build stage
FROM node:18-alpine AS build
WORKDIR /home/node/app
COPY taranta/package*.json ./
RUN npm install
COPY taranta ./
RUN npm run build

# Serve stage
FROM nginx:alpine
# Install nano and gawk
RUN apk add --no-cache nano gawk
COPY --from=build /home/node/app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/
COPY namespace.sh /namespace.sh
COPY reload-nginx.sh /etc/periodic/1800sec/reload-nginx
RUN chmod +x /namespace.sh
RUN chmod +x /etc/periodic/1800sec/reload-nginx
ENTRYPOINT ["/namespace.sh"]
