.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Home
  :hidden:

SKA Taranta Pipeline
--------------------

Welcome to the SKA Taranta Pipeline ReadTheDocs

This docs are for SKA developers use only, if you are searching for taranta software
ReadTheDocs please go to |Taranta_Docs|

Introduction to Taranta Pipeline
--------------------------------
.. INTRODUCTION SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Introduction
  :hidden:

  introduction

- :doc:`introduction`

.. |Taranta_Docs| raw:: html

  <a 
  href="https://taranta.readthedocs.io/en/latest/" 
  target="_blank">Taranta Docs</a>