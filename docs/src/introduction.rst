
Introduction
=============

As a SKA developer with Taranta Pipeline you can:

* Build new docker images
* Build new charts
* Trigger namespaces changes to https://k8s.stfc.skao.int/taranta-namespace/taranta 
* Create/Edit developer doc pages
